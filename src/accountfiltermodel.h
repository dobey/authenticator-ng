/*
 * Copyright © 2021 Rodney Dawes
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#pragma once

#include <QCollator>
#include <QSortFilterProxyModel>

class AccountFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit AccountFilterModel(QObject *parent=0);

    Q_INVOKABLE void setFilterName(const QString& name);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const override;
    bool lessThan(const QModelIndex &left,
                  const QModelIndex &right) const override;


private:
    QString m_filteredName;
    QCollator m_collator;
};
