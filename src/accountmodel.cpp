/*
 * Copyright © 2020 Rodney Dawes
 * Copyright: 2013 Michael Zanetti <michael_zanetti@gmx.net>
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "accountmodel.h"

#include <QDebug>
#include <QSettings>
#include <QStringList>

AccountModel::AccountModel(const QSettings& settings, QObject *parent) :
    QAbstractListModel(parent),
    m_settings(settings.fileName(), QSettings::IniFormat)
{
    refresh();
}

int AccountModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_accounts.count();
}

QVariant AccountModel::data(const QModelIndex &index, int role) const
{
    if (index.isValid()) {
        auto account = m_accounts.at(index.row());
        switch (role) {
        case Roles::UID:
            return account->uid();
        case Roles::Name:
            return account->name();
        case Roles::Type:
            return account->type();
        case Roles::Secret:
            return account->secret();
        case Roles::Counter:
            return account->counter();
        case Roles::TimeStep:
            return account->timeStep();
        case Roles::PinLength:
            return account->pinLength();
        case Roles::Algorithm:
            return account->algorithm();
        case Roles::Otp:
            return account->otp();
        default:
            qWarning() << "Unhandled role:" << role;
        }
    } else {
        qCritical() << "Invalid index.";
    }

    return QVariant();
}

QHash<int, QByteArray> AccountModel::roleNames() const
{
    static QHash<int, QByteArray> roles;
    if (roles.empty()) {
        roles.insert(Roles::UID, "uid");
        roles.insert(Roles::Name, "name");
        roles.insert(Roles::Type, "type");
        roles.insert(Roles::Secret, "secret");
        roles.insert(Roles::Counter, "counter");
        roles.insert(Roles::TimeStep, "timeStep");
        roles.insert(Roles::PinLength, "pinLength");
        roles.insert(Roles::Algorithm, "algorithm");
        roles.insert(Roles::Otp, "otp");
    }
    return roles;
}

Account *AccountModel::get(const QString& uid) const
{
    for (const auto& account: m_accounts) {
        if (account->uid() == uid) {
            return account;
        }
    }
    return nullptr;
}

void AccountModel::remove(Account* account)
{
    auto uid = account->uid();
    auto index = indexOf(uid);
    beginRemoveRows(QModelIndex(), index, index);
    m_accounts.removeAt(index);
    m_settings.beginGroup(uid);
    m_settings.remove("");
    m_settings.endGroup();
    endRemoveRows();
    emit countChanged();
}

void AccountModel::save(Account* account)
{
    account->setParent(this);
    auto uid = account->uid();

    m_settings.beginGroup(uid);
    m_settings.setValue("account", account->name());
    m_settings.setValue("type", account->type() == Account::Type::TOTP ? "totp" : "hotp");
    m_settings.setValue("secret", account->secret());
    m_settings.setValue("counter", account->counter());
    m_settings.setValue("timeStep", account->timeStep());
    m_settings.setValue("pinLength", account->pinLength());
    m_settings.setValue("algorithm", account->algorithm());
    m_settings.endGroup();

    auto oldAccount = get(uid);
    if (oldAccount != nullptr) {
        oldAccount->setName(account->name());
        oldAccount->setType(account->type());
        oldAccount->setSecret(account->secret());
        oldAccount->setCounter(account->counter());
        oldAccount->setTimeStep(account->timeStep());
        oldAccount->setPinLength(account->pinLength());
        oldAccount->setAlgorithm(account->algorithm());
        auto idx = index(indexOf(uid));
        emit dataChanged(idx, idx,
                         QVector<int>() << Roles::Name << Roles::Type <<
                         Roles::Secret << Roles::Counter << Roles::TimeStep <<
                         Roles::PinLength << Roles::Algorithm << Roles::Otp);
    } else {
        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        m_accounts.append(account);
        endInsertRows();
        emit countChanged();
    }
}

Account *AccountModel::createAccount(const QString& text)
{
    Account *account = nullptr;

    if (text.isEmpty()) {
        account = new Account(QUuid::createUuid(), this);
    } else {
        account = Account::fromURL(text);
    }
    return account;
}

void AccountModel::generateNext(int account)
{
    m_accounts.at(account)->next();
    emit dataChanged(index(account), index(account), QVector<int>() << Roles::Counter << Roles::Otp);
}

void AccountModel::refresh()
{
    beginResetModel();
    endResetModel();
    for (const auto& group: m_settings.childGroups()) {
        QUuid id = QUuid(group);

        m_settings.beginGroup(group);
        Account *account = new Account(id, this);
        account->setName(m_settings.value("account").toString());
        account->setType(m_settings.value("type", "hotp").toString() == "totp" ? Account::Type::TOTP : Account::Type::HOTP);
        account->setSecret(m_settings.value("secret").toString());
        account->setCounter(m_settings.value("counter").toInt());
        account->setTimeStep(m_settings.value("timeStep").toInt());
        account->setPinLength(m_settings.value("pinLength", 6).toInt());
        account->setAlgorithm(static_cast<Account::Algorithm>(m_settings.value("algorithm", Account::Algorithm::SHA1).toInt()));

        if (account->type() == Account::Type::HOTP) {
            connect(account, &Account::counterChanged, [this, account] () {
                save(account);
            });
        }

        m_settings.endGroup();

        beginInsertRows(QModelIndex(), rowCount(), rowCount());
        m_accounts << account;
        endInsertRows();
        emit countChanged();
    }
}

int AccountModel::indexOf(const QString& uid)
{
    int i = 0;
    for (const auto& account: m_accounts) {
        if (account->uid() == uid) {
            return i;
        }
        i++;
    }
    return -1;
}
