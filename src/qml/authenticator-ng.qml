/*
 * Copyright © 2018-2020 Rodney Dawes
 * Copyright: 2013 Michael Zanetti <michael_zanetti@gmx.net>
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import Ergo 0.0
import QtQuick 2.12
import QtQuick.Layouts 1.1
import OAth 1.0
import QtMultimedia 5.8
import QtQuick.Controls 2.12
import QtQuick.Window 2.0
import "."

ApplicationWindow {
    id: root

    title: i18n.tr("Authenticator")
    visible: true

    minimumWidth: 360
    minimumHeight: 640

    Gettext {
        domain: "authenticator-ng"
    }

    Shortcut {
        context: Qt.ApplicationShortcut
        sequence: StandardKey.Quit
        onActivated: {
            Qt.quit();
        }
    }

    StackView {
        id: pageStack
        anchors.fill: parent
        visible: true
        anchors.margins: 0
        focus: true
        Component.onCompleted: {
            pageStack.push(mainPageComponent);
        }
        Keys.onBackPressed: {
            if (depth > 1) {
                pop();
            } else {
                Qt.quit();
            }
        }
    }

    Component {
        id: mainPageComponent
        MainPage {
            id: mainPage
            visible: false

            onScanCode: {
                pageStack.push(scanPageComponent);
            }

            onAddToken: {
                pageStack.push(editPageComponent);
            }

            onEditToken: {
                pageStack.push(editPageComponent, {account: token});
            }
        }
    }

    Component {
        id: editPageComponent

        EditPage {
            id: editPage

            onSave: {
                AccountModel.save(account);
                pageStack.pop();
            }

            onClose: {
                pageStack.pop();
            }
        }
    }

    Component {
        id: scanPageComponent

        ScanPage {
            id: scanPage

            onSave: {
                AccountModel.save(account);
                pageStack.pop();
            }

            onClose: {
                pageStack.pop();
            }
        }
    }
}
