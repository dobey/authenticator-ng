/*
 * Copyright © 2018-2020 Rodney Dawes
 * Copyright: 2013 Michael Zanetti <michael_zanetti@gmx.net>
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import Ergo 0.0
import OAth 1.0
import QtQuick 2.12
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.12

Page {
    id: editPage

    property QtObject account: null
    readonly property bool validData: {
        return (nameField.displayText.length > 0 &&
            secretField.acceptableInput && counterField.acceptableInput &&
            timeStepField.acceptableInput);
    }

    signal close()
    signal save(Account account)

    header: AdaptiveToolbar {
        id: editToolbar
        width: editPage.width
        height: 48

        leadingActions: [
            Action {
                icon.name: "go-previous-symbolic"
                shortcut: [StandardKey.Back, StandardKey.Cancel]
                onTriggered: {
                    editPage.close();
                }
            }
        ]
        trailingActions: [
            Action {
                enabled: editPage.validData
                icon.name: "document-save-symbolic"
                text: i18n.tr("Save account")
                shortcut: StandardKey.Save
                onTriggered: {
                    var newAccount = account;
                    if (newAccount == null) {
                        newAccount = AccountModel.createAccount();
                    }

                    newAccount.name = nameField.text;
                    newAccount.type = typeSelector.currentIndex == 1 ? Account.TOTP : Account.HOTP;
                    newAccount.secret = secretField.text;
                    newAccount.counter = parseInt(counterField.text);
                    newAccount.timeStep = parseInt(timeStepField.text);
                    newAccount.pinLength = parseInt(pinLengthField.text);
                    newAccount.algorithm = algoSelector.currentIndex << 8;
                    editPage.save(newAccount);
                }
            }
        ]
        Label {
            anchors.fill: parent
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            text: account == null ? i18n.tr("Add account") : i18n.tr("Edit account")
            font.pixelSize: 24
        }
    }

    Flickable {
        id: settingsFlickable
        anchors.fill: parent
        contentHeight: settingsColumn.height + (settingsColumn.anchors.margins * 2)

        Column {
            id: settingsColumn
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
                margins: 16
            }
            spacing: 16

            Label {
                text: i18n.tr("Name")
                font.pixelSize: 18
            }
            TextField {
                id: nameField
                width: parent.width
                height: 36
                font.pixelSize: 14
                text: account ? account.name : ""
                selectByMouse: true
                placeholderText: i18n.tr("Enter the account name")
                inputMethodHints: Qt.ImhNoPredictiveText
            }

            Label {
                text: i18n.tr("Type")
                font.pixelSize: 18
            }

            ComboBox {
                id: typeSelector
                width: parent.width
                height: 36
                editable: false
                model: [i18n.tr("Counter based"), i18n.tr("Time based")]
                currentIndex: account && account.type === Account.HOTP ? 0 : 1
            }

            ComboBox {
                id: algoSelector
                width: parent.width
                height: 36
                editable: false
                visible: typeSelector.currentIndex == 1
                model: ["SHA-1", "SHA-256", "SHA-512"]
                currentIndex: account ? account.algorithm >> 8 : 0
            }

            Label {
                text: i18n.tr("Key")
                font.pixelSize: 18
            }
            TextField {
                id: secretField
                width: parent.width
                height: 36
                font.pixelSize: 14
                // Between 16-240 characters, not starting with = sign
                validator: RegExpValidator { regExp: /^([^=].*){16,240}$/ }
                text: account ? account.secret : ""
                selectByMouse: true
                wrapMode: Text.NoWrap
                // TRANSLATORS: placeholder text in key textfield
                placeholderText: i18n.tr("Enter the valid secret key")
                inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
            }
            Row {
                width: parent.width
                spacing: 4
                visible: typeSelector.currentIndex == 0

                Label {
                    text: i18n.tr("Counter")
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 18
                }
                TextField {
                    id: counterField
                    validator: IntValidator {
                        bottom: 0
                        locale: "C"
                    }
                    text: account ? account.counter : 0
                    width: parent.width - x
                    height: 36
                    font.pixelSize: 14
                    inputMethodHints: Qt.ImhDigitsOnly
                    selectByMouse: true
                }
            }
            Row {
                width: parent.width
                spacing: 4
                visible: typeSelector.currentIndex == 1

                Label {
                    text: i18n.tr("Time step")
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 18
                }
                TextField {
                    id: timeStepField
                    validator: IntValidator {
                        bottom: 10
                        locale: "C"
                    }
                    text: account ? account.timeStep : 30
                    width: parent.width - x
                    height: 36
                    font.pixelSize: 14
                    inputMethodHints: Qt.ImhDigitsOnly
                    selectByMouse: true
                }
            }
            Row {
                width: parent.width
                spacing: 4

                Label {
                    text: i18n.tr("PIN length")
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: 18
                }
                SpinBox {
                    id: pinLengthField
                    from: 6
                    // Apparently some use 10, though RFC says 6-8
                    to: 10
                    value: account ? account.pinLength : 6
                    width: parent.width - x
                    height: 36
                }
            }
            Item {
                width: parent.width
                height: Qt.inputMethod.keyboardRectangle.height
            }
        }
    }

}
