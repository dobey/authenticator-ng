/*
 * Copyright © 2018-2020 Rodney Dawes
 * Copyright: 2013 Michael Zanetti <michael_zanetti@gmx.net>
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import Ergo 0.0
import OAth 1.0
import QtQuick 2.12
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.12

Item {
    id: mainRect

    property alias token: removeDialog.token

    signal accepted()
    signal rejected()

    function open() {
        removeDialog.open();
    }

    function close() {
        removeDialog.close();
    }

    visible: removeDialog && removeDialog.opened

    Popup {
        id: removeDialog

        property QtObject token: null

        x: parent.width / 2 - width / 2
        y: parent.height / 2 - height / 2

        width: parent.width - 48
        padding: 12

        modal: true
        closePolicy: Popup.CloseOnEscape

        onClosed: {
            destroy();
        }

        background: Rectangle {
            color: "#111111"
            opacity: 0.99
            radius: 8
        }

        ColumnLayout {
            width: parent.width
            spacing: 12

            Label {
                Layout.fillWidth: true
                text: i18n.tr("Remove account?")
                font.pixelSize: 24
                wrapMode: Text.WordWrap
            }

            Label {
                Layout.fillWidth: true
                text: i18n.tr("Are you sure you want to remove the '%1' account?").arg(token ? token.name : "")
                font.pixelSize: 14
                wrapMode: Text.WordWrap
            }

            Item {
                Layout.fillWidth: true
                height: 16
            }

            RowLayout {
                Layout.alignment: Qt.AlignRight
                spacing: 24

                Button {
                    Layout.fillWidth: true
                    font.pixelSize: 16
                    text: i18n.tr("Cancel")
                    onClicked: {
                        mainRect.rejected();
                        removeDialog.close();
                    }
                }
                Button {
                    Layout.fillWidth: true
                    font.pixelSize: 16
                    text: i18n.tr("Remove")
                    onClicked: {
                        mainRect.accepted();
                        removeDialog.close();
                    }
                    background: Rectangle {
                        color: "#ed3434"
                        radius: 10
                    }
                }
            }
        }
    }
}
