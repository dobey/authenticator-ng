/*
 * Copyright © 2018-2020 Rodney Dawes
 * Copyright: 2013 Michael Zanetti <michael_zanetti@gmx.net>
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import Ergo 0.0
import OAth 1.0
import QtMultimedia 5.8
import QtQuick 2.12
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.12
import QtQuick.Window 2.0

import QZXing 3.3

Page {
    id: scanPage

    signal close()
    signal save(Account account)

    header: AdaptiveToolbar {
        width: parent.width
        height: 48

        leadingActions: [
            Action {
                icon.name: "go-previous-symbolic"
                shortcut: [StandardKey.Back, StandardKey.Cancel]
                onTriggered: {
                    scanPage.close();
                }
            }
        ]
        Label {
            anchors.fill: parent
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            text: i18n.tr("Scan QR code")
            font.pixelSize: 24
        }
    }

    Camera {
        id: camera

        focus.focusMode: Camera.FocusContinuous
        focus.focusPointMode: Camera.FocusPointCenter

        exposure.exposureMode: Camera.ExposureBarcode
        exposure.meteringMode: Camera.MeteringSpot

        imageProcessing.sharpeningLevel: 0.5
        imageProcessing.denoisingLevel: 0.25

        viewfinder.minimumFrameRate: 30.0
        viewfinder.maximumFrameRate: 30.0

        Component.onCompleted: {
            // This should not be necessary, but somehow makes focus work
            // properly on Ubuntu Touch. qtubuntu-camera bug perhaps? May
            // also be needed on Android for Qt?
            unlock();
            focus.focusMode = Camera.FocusContinuous;
            focus.focusPointMode = Camera.FocusPointCenter;
            searchAndLock();
        }
    }

    Timer {
        id: captureTimer
        interval: 250
        repeat: true
        running: Qt.application.active
        onTriggered: {
            videoOutput.grabToImage(function(result) {
                qrCodeReader.decodeImage(result.image);
            });
        }
    }

    VideoOutput {
        id: videoOutput

        anchors.fill: parent
        fillMode: VideoOutput.PreserveAspectCrop
        source: camera
        focus: true
        orientation: Screen.primaryOrientation == Qt.PortraitOrientation ? -90 : 0
    }

    Canvas {
        id: zoneOverlay
        anchors.fill: parent

        onPaint: {
            let ctx = getContext("2d");
            ctx.save();
            ctx.reset();

            ctx.fillStyle = Qt.rgba(0.0, 0.0, 0.0, 0.93);

            ctx.beginPath();
            ctx.fillRect(0, 0, width, height);
            ctx.fill();

            let size = (width > height ? height : width) * 0.75;
            let rx = (width - size) / 2;
            let ry = (height - size) / 2;
            ctx.clearRect(rx, ry, size, size);

            ctx.restore();
        }
    }

    QZXing {
        id: qrCodeReader

        enabledDecoders: QZXing.DecoderFormat_QR_CODE

        onTagFound: {
            captureTimer.stop();
            camera.stop();
            var account = AccountModel.createAccount(tag);
            if (account == null) {
                var popup = errorComponent.createObject(
                    scanPage,
                    {
                        summary: i18n.tr("Invalid Code Scanned"),
                        message: i18n.tr("The scanned QR code is not valid.")
                    }
                );
                popup.closed.connect(function() {
                    camera.start();
                    captureTimer.start();
                });
                popup.open();
            } else {
                scanPage.save(account);
            }
        }

        tryHarder: false
    }

    Label {
        id: scanLabel
        anchors {
            left: parent.left
            top: parent.top
            right: parent.right
            margins: 4
        }

        opacity: 0.93
        width: parent.width
        padding: 4
        y: 4
        text: i18n.tr("Scan a QR Code containing account information")
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 16
    }

    Component {
        id: errorComponent

        ErrorDialog {
            anchors.fill: parent
        }
    }
}
