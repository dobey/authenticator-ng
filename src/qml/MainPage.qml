/*
 * Copyright © 2018-2022 Rodney Dawes
 * Copyright: 2013 Michael Zanetti <michael_zanetti@gmx.net>
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import Ergo 0.0
import OAth 1.0
import QtQuick 2.12
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.12
import QtQuick.Window 2.0

Page {
    id: mainPage

    signal addToken()
    signal editToken(var token)
    signal scanCode()

    header: AdaptiveToolbar {
        id: mainToolbar
        width: mainPage.width
        height: 48

        trailingActions: [
            Action {
                icon.name: searchField.visible ? "go-previous-symbolic" : "edit-find-symbolic"
                shortcut: searchField.visible ? StandardKey.Cancel : [StandardKey.Find, StandardKey.FindNext]
                onTriggered: {
                    if (searchField.visible) {
                        searchField.text = "";
                        AccountFilterModel.setFilterName("");
                        mainPage.state = "";
                    } else {
                        mainPage.state = "searching";
                        searchField.forceActiveFocus();
                    }
                }
            },
            Action {
                icon.name: "camera-photo-symbolic"
                text: i18n.tr("Scan QR code")
                shortcut: "Ctrl+Shift+N"
                onTriggered: {
                    mainPage.scanCode();
                }
            },
            Action {
                icon.name: "list-add-symbolic"
                text: i18n.tr("Add account")
                shortcut: StandardKey.New
                onTriggered: {
                    mainPage.addToken();
                }
            }
        ]

        TextField {
            id: searchField
            width: parent.width
            anchors.verticalCenter: parent.verticalCenter
            placeholderText: i18n.tr("Search")
            visible: mainPage.state === "searching"
            onTextChanged: {
                AccountFilterModel.setFilterName(text);
            }
            onPreeditTextChanged: {
                AccountFilterModel.setFilterName(preeditText);
            }
        }
    }

    states: [
        State {
            name: ""
        },
        State {
            name: "searching"
        }
    ]

    Clipboard {
        id: clipboard
    }

    Label {
        anchors.centerIn: parent
        width: parent.width - 40
        text: i18n.tr("No account set up. Use the buttons in the toolbar to add accounts.")
        wrapMode: Text.WordWrap
        font.pixelSize: 24
        horizontalAlignment: Text.AlignHCenter
        visible: AccountModel.count == 0
    }

    Popup {
        id: popover
        padding: 12

        x: parent.width / 2 - width / 2
        y: parent.height - height - 12

        background: Rectangle {
            color: "#111111"
            opacity: 0.93
            radius: 10
        }

        Text {
            id: copiedLabel
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            color: "#ffffff"
            text: i18n.tr("Code copied to clipboard")
            font.pixelSize: 16
        }

        Timer {
            id: popupTimer
            interval: 3000
            running: true
            onTriggered: {
                popover.close();
            }
        }

        function show() {
            open();
            popupTimer.start();
        }
    }

    ListView {
        id: accountsListView
        anchors.fill: parent
        spacing: 8
        model: AccountFilterModel
        interactive: contentHeight > height

        delegate: AdaptiveListItem {
            id: accountDelegate
            width: accountsListView.width
            height: 56

            property bool activated: false
            readonly property Account account: AccountModel.get(uid)

            actions: [
                Action {
                    icon.name: "edit-copy-symbolic"
                    text: i18n.tr("Copy")
                    enabled: accountDelegate.activated || account.type === Account.TOTP
                    shortcut: StandardKey.Copy
                    onTriggered: {
                        accountDelegate.copyToClipBoard()
                    }
                },
                Action {
                    icon.name: "document-edit-symbolic"
                    text: i18n.tr("Edit")
                    onTriggered: {
                        mainPage.editToken(account)
                    }
                },
                Action {
                    icon.name: "edit-delete-symbolic"
                    text: i18n.tr("Remove")
                    shortcut: StandardKey.Delete
                    onTriggered: {
                        var popup = removeComponent.createObject(
                                mainPage,
                                {token: account});
                        popup.accepted.connect(function() {
                            AccountModel.remove(account);
                        });
                        popup.rejected.connect(function() {
                        });
                        popup.open();
                    }
                }
            ]

            function copyToClipBoard() {
                clipboard.pushData(otpLabel.text);
                popover.show();
            }

            GridLayout {
                id: delegateColumn
                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                    leftMargin: 16
                    topMargin: 8
                    rightMargin: refreshButton.width + 24
                }
                rowSpacing: 4
                columnSpacing: 8
                height: parent.height - (anchors.topMargin * 2)
                columns: 1

                Label {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    text: account.name
                    font.pixelSize: 14
                    elide: Text.ElideRight
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                }

                Label {
                    id: otpLabel
                    Layout.fillHeight: true
                    Layout.preferredWidth: delegateColumn.width
                    font.family: "mono"
                    font.pixelSize: 22
                    text: accountDelegate.activated || account.type === Account.TOTP ? account.otp : "------"
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter

                    MouseArea {
                        id: copy
                        anchors {
                            left: parent.left
                            bottom: parent.bottom
                        }
                        width: parent.contentWidth
                        height: parent.contentHeight
                        onClicked: {
                            accountDelegate.copyToClipBoard();
                        }
                    }
                }
            }

            Item {
                id: refreshButton
                anchors {
                    right: parent.right
                    rightMargin: 8
                    verticalCenter: parent.verticalCenter
                }
                height: parent.height
                width: height

                Icon {
                    anchors.centerIn: parent
                    name: "view-refresh-symbolic"
                    visible: account.type === Account.HOTP
                    height: parent.height - 16
                    width: height
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            AccountModel.generateNext(index);
                            accountDelegate.activated = true;
                        }
                    }
                }

                Item {
                    id: progressCircle
                    anchors.centerIn: parent
                    height: parent.height - 16
                    width: height
                    visible: account.type === Account.TOTP
                    property real progress: 0

                    Timer {
                        interval: 100
                        running: account.type === Account.TOTP
                        repeat: true
                        onTriggered: {
                            var duration = account.msecsToNext();
                            progressCircle.progress = ((timeStep * 1000) - duration) / (timeStep * 1000)
                        }
                    }

                    Canvas {
                        id: canvas
                        anchors.fill: parent
                        rotation: -90

                        property real scale: Screen.devicePixelRatio >= 1.0 ? 1.0 : Screen.devicePixelRatio

                        property real progress: progressCircle.progress
                        onProgressChanged: {
                            canvas.requestPaint();
                        }

                        onPaint: {
                            var ctx = canvas.getContext("2d");
                            ctx.save();
                            ctx.reset();
                            var data = [1 - progress, progress];
                            var myTotal = 0;

                            for (var e = 0; e < data.length; e++) {
                                myTotal += data[e];
                            }

                            ctx.beginPath();
                            ctx.lineWidth = 4;
                            ctx.strokeStyle = otpLabel.color
                            ctx.arc(canvas.width / 2 * scale, canvas.height / 2 * scale,
                                    (canvas.height / 2 - 4)* scale, 0,
                                    (Math.PI * 2 * ((1 - progress) / myTotal)),
                                    false);
                            ctx.stroke();

                            ctx.restore();
                        }
                    }
                }
            }
        }
    }

    Component {
        id: removeComponent

        RemoveDialog {
            height: parent.height
            width: parent.width
        }
    }
}
