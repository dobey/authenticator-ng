/*
 * Copyright © 2020 Rodney Dawes
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "../accountmodel.h"

#include <QDebug>
#include <QFile>
#include <QtTest/QtTest>


class TestModel: public QObject
{
    Q_OBJECT

private slots:
    void init();

    void model();

private:
    QTemporaryFile* m_tmpFile;
    AccountModel* m_model;
};

void TestModel::init()
{
    m_tmpFile = new QTemporaryFile(QStringLiteral("modelXXXXXX.conf"), this);
    m_tmpFile->open();
    m_tmpFile->close();
    QFile::remove(m_tmpFile->fileName());
    QFile::copy(TEST_MODEL_CONF, m_tmpFile->fileName());
    m_model = new AccountModel(QSettings(m_tmpFile->fileName(),
                                         QSettings::IniFormat),
                               this);
}

void TestModel::model()
{
    QCOMPARE(m_model->rowCount(QModelIndex()), 2);
    QCOMPARE(m_model->roleNames().count(), 9);

    auto account1 = m_model->createAccount();
    m_model->save(account1);
    auto account2 = m_model->createAccount();
    m_model->save(account2);
    account2->setName("Account 2");
    m_model->save(account2);
    QCOMPARE(m_model->rowCount(QModelIndex()), 4);
    m_model->remove(account1);
    m_model->remove(account2);
    QCOMPARE(m_model->rowCount(QModelIndex()), 2);

    auto account3 = m_model->createAccount();
    m_model->save(account3);
    QCOMPARE(m_model->rowCount(QModelIndex()), 3);
    auto fetched = m_model->get(account3->uid());
    QCOMPARE(fetched, account3);
    fetched->setName("New name");
    m_model->generateNext(0);

    for (int role = AccountModel::Roles::UID; role <= AccountModel::Roles::Otp; ++role) {
        auto data = m_model->data(m_model->index(0, 0), role);
        QVERIFY(data.isValid());
    }
    QVERIFY(!m_model->data(m_model->index(0, 0), Qt::UserRole).isValid());

    auto invalid = m_model->data(QModelIndex(), 999);
    QVERIFY(!invalid.isValid());

    auto invalidacct = m_model->createAccount();
    invalidacct->setUid("notarealuid");
    m_model->remove(invalidacct);

    QCOMPARE(m_model->get("invaliduid"), nullptr);

    QCOMPARE(m_model->createAccount("https://example.com"), nullptr);
}

QTEST_MAIN(TestModel)
#include "test-model.moc"
