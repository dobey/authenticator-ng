/*
 * Copyright © 2021 Rodney Dawes
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "../accountfiltermodel.h"
#include "../accountmodel.h"

#include <QFile>
#include <QtTest/QtTest>


class TestFilterModel: public QObject
{
    Q_OBJECT

private slots:
    void init();

    void filterModel();

private:
    QTemporaryFile* m_tmpFile;
    AccountModel* m_model;
};

void TestFilterModel::init()
{
    m_tmpFile = new QTemporaryFile(QStringLiteral("filterModelXXXXXX.conf"), this);
    m_tmpFile->open();
    m_tmpFile->close();
    QFile::remove(m_tmpFile->fileName());
    QFile::copy(TEST_MODEL_CONF, m_tmpFile->fileName());
    m_model = new AccountModel(QSettings(m_tmpFile->fileName(),
                                         QSettings::IniFormat),
                               this);
}

void TestFilterModel::filterModel()
{
    auto filterModel = new AccountFilterModel;
    filterModel->setSourceModel(m_model);

    QCOMPARE(filterModel->rowCount(), 2);

    filterModel->setFilterName("alice");
    QCOMPARE(filterModel->rowCount(), 1);
}

QTEST_MAIN(TestFilterModel)
#include "test-filtermodel.moc"
