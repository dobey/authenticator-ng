/*
 * Copyright © 2021 Rodney Dawes
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "accountfiltermodel.h"
#include "accountmodel.h"

AccountFilterModel::AccountFilterModel(QObject *parent) :
    QSortFilterProxyModel(parent),
    m_collator(QCollator())
{
    m_collator.setCaseSensitivity(Qt::CaseInsensitive);
    sort(0);
}

void AccountFilterModel::setFilterName(const QString& name)
{
    m_filteredName = name;
    invalidateFilter();
}

bool AccountFilterModel::filterAcceptsRow(int sourceRow,
                                          const QModelIndex& sourceParent) const
{
    if (m_filteredName.isEmpty()) {
        return true;
    }

    auto index0 = sourceModel()->index(sourceRow, 0, sourceParent);
    return sourceModel()->data(index0, AccountModel::Roles::Name).toString().contains(m_filteredName, Qt::CaseInsensitive);
}

bool AccountFilterModel::lessThan(const QModelIndex &left,
                                  const QModelIndex &right) const
{
    QString leftName = sourceModel()->data(left, AccountModel::Roles::Name).toString();
    QString rightName = sourceModel()->data(right, AccountModel::Roles::Name).toString();

    return m_collator.compare(leftName, rightName) < 0;
}
