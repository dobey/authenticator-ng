# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-03 16:53-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: NORWEGIAN <LL@li.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../app/qml/authenticator-ng.qml:32
msgid "Authenticator"
msgstr ""

#: ../app/qml/authenticator-ng.qml:83 ../app/qml/authenticator-ng.qml:709
msgid "Scan QR code"
msgstr "Skann QR-kode"

#: ../app/qml/authenticator-ng.qml:92 ../app/qml/authenticator-ng.qml:414
msgid "Add account"
msgstr "Legg til konto"

#: ../app/qml/authenticator-ng.qml:110
msgid "No account set up. Use the buttons in the toolbar to add accounts."
msgstr "Ingen konto satt opp. Bruk knappene i verktøylinjen for å legge til kontoer."

#: ../app/qml/authenticator-ng.qml:136
msgid "Code copied to clipboard"
msgstr "Kode kopiert til utklippstavlen"

#: ../app/qml/authenticator-ng.qml:172
msgid "Copy"
msgstr "Kopier"

#: ../app/qml/authenticator-ng.qml:181
msgid "Edit"
msgstr "Rediger"

#: ../app/qml/authenticator-ng.qml:188 ../app/qml/authenticator-ng.qml:928
msgid "Remove"
msgstr "Fjern"

#: ../app/qml/authenticator-ng.qml:392
msgid "Save account"
msgstr "Lagre konto"

#: ../app/qml/authenticator-ng.qml:414
msgid "Edit account"
msgstr "Rediger konto"

#: ../app/qml/authenticator-ng.qml:449
msgid "Name"
msgstr "Navn"

#: ../app/qml/authenticator-ng.qml:467
msgid "Enter the account name"
msgstr "Tast inn kontonavn"

#: ../app/qml/authenticator-ng.qml:472
msgid "Type"
msgstr "Type"

#: ../app/qml/authenticator-ng.qml:482
msgid "Counter based"
msgstr "Tellerbasert"

#: ../app/qml/authenticator-ng.qml:482
msgid "Time based"
msgstr "Tidsbasert"

#: ../app/qml/authenticator-ng.qml:559
msgid "Key"
msgstr "Nøkkel"

#. TRANSLATORS: placeholder text in key textfield
#: ../app/qml/authenticator-ng.qml:579
msgid "Enter the 16 or 32 digit key"
msgstr "Tast inn den 16 eller 32 sifrede nøkkelen"

#: ../app/qml/authenticator-ng.qml:588
msgid "Counter"
msgstr "Teller"

#: ../app/qml/authenticator-ng.qml:617
msgid "Time step"
msgstr "Tidstrinn"

#: ../app/qml/authenticator-ng.qml:645
msgid "PIN length"
msgstr "PIN lengde"

#: ../app/qml/authenticator-ng.qml:819
msgid "Scan a QR Code containing account information"
msgstr "Skann en QR-kode som inneholder kontoinformasjon"

#: ../app/qml/authenticator-ng.qml:893
msgid "Remove account?"
msgstr "Fjern konto"

#: ../app/qml/authenticator-ng.qml:901
msgid "Are you sure you want to remove the '%1' account?"
msgstr "Er du sikker på at du vil fjerne '%1'-kontoen?"

#: ../app/qml/authenticator-ng.qml:919
msgid "Cancel"
msgstr "Avbryt"
