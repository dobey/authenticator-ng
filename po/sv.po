# Swedish translation for authenticator
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the authenticator package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: authenticator\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2015-09-10 00:34+0200\n"
"PO-Revision-Date: 2015-04-22 06:14+0000\n"
"Last-Translator: Tommy Brunn <Unknown>\n"
"Language-Team: Swedish <sv@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2016-05-21 05:44+0000\n"
"X-Generator: Launchpad (build 18025)\n"

#: app/qml/authenticator-ng.qml:58
#: app/qml/authenticator-ng.qml:339
msgid "Add account"
msgstr "Lägg till konto"

#: app/qml/authenticator-ng.qml:65
#: app/qml/authenticator-ng.qml:485
msgid "Scan QR code"
msgstr "Skanna QR-kod"

#: app/qml/authenticator-ng.qml:77
msgid "No account set up. Use the buttons in the toolbar to add accounts."
msgstr ""
"Inga konton uppsatta. Använd knapparna i verktygsraden för att lägga till "
"konton."

#: app/qml/authenticator-ng.qml:123
msgid "Delete"
msgstr "Ta bort"

#: app/qml/authenticator-ng.qml:137
msgid "Copy"
msgstr ""

#: app/qml/authenticator-ng.qml:145
msgid "Edit"
msgstr "Redigera"

#: app/qml/authenticator-ng.qml:173
msgid "Copied"
msgstr ""

#. TRANSLATORS: Text on a button
#: app/qml/authenticator-ng.qml:240
msgid "Generate PIN"
msgstr "Generera PIN"

#: app/qml/authenticator-ng.qml:339
msgid "Edit account"
msgstr "Redigera konto"

#: app/qml/authenticator-ng.qml:383
msgid "Name"
msgstr "Namn"

#: app/qml/authenticator-ng.qml:389
msgid "Enter the account name"
msgstr "Fyll i kontots namn"

#: app/qml/authenticator-ng.qml:394
msgid "Type"
msgstr "Typ"

#: app/qml/authenticator-ng.qml:400
msgid "Counter based"
msgstr "Räknarbaserad"

#: app/qml/authenticator-ng.qml:400
msgid "Time based"
msgstr "Tidsbaserad"

#: app/qml/authenticator-ng.qml:405
msgid "Key"
msgstr "Nyckel"

#. TRANSLATORS: placeholder text in key textfield
#: app/qml/authenticator-ng.qml:414
msgid "Enter the 16 or 32 digit key"
msgstr ""

#: app/qml/authenticator-ng.qml:423
msgid "Counter"
msgstr "Räknare"

#: app/qml/authenticator-ng.qml:440
msgid "Time step"
msgstr "Tidssteg"

#: app/qml/authenticator-ng.qml:456
msgid "PIN length"
msgstr "PIN-längd"

#: app/qml/authenticator-ng.qml:544
msgid "Scan a QR Code containing account information"
msgstr "Skanna en QR-kod innehållandes kontoinformation"

#: app/qml/authenticator-ng.qml:563
msgid "Remove account?"
msgstr "Ta bort konto?"

#: app/qml/authenticator-ng.qml:564
#, qt-format
msgid "Are you sure you want to remove %1?"
msgstr "Är du säker på att du vill ta bort %1?"

#: app/qml/authenticator-ng.qml:572
msgid "Yes"
msgstr "Ja"

#: app/qml/authenticator-ng.qml:580
msgid "Cancel"
msgstr "Avbryt"
